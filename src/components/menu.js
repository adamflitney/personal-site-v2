import React from "react"
import { Link } from "gatsby"

const Menu = () => (
  <ul>
    <Link to="/">
      <li>Home</li>
    </Link>
    <Link to="/about">
      <li>About</li>
    </Link>
    <Link to="/work">
      <li>Work</li>
    </Link>
    <Link to="/contact">
      <li>Contact</li>
    </Link>
  </ul>
)
export default Menu
