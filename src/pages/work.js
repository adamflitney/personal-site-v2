import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import jsIcon from "../images/js.png"
import vueIcon from "../images/vuejs.png"
import reactIcon from "../images/react.png"
import vsCodeIcon from "../images/vs-code.png"
import gitlabIcon from "../images/gitlab.png"
import githubIcon from "../images/github.png"
import xliffSite from "../images/xliff-editor-ss.png"
import personalSite from "../images/website-v2-ss.png"
import pomodoroApp from "../images/pomo-ss.png"


const WorkPage = () => (
  <Layout>
    <SEO title="Work" />
    <h2>Work & Projects</h2>
    <h3>Main Technologies I Use</h3>
    <div className="technologies">
      <div className="languages">
        <img src={jsIcon} alt="JS Icon" title="Javascript" />
        <img src={vueIcon} alt="Vue JS Icon" title="Vue" />
        <img src={reactIcon} alt="React JS Icon" title="React" />
      </div>
      <div className="tools">
        <img src={vsCodeIcon} alt="VS Code Icon" title="VS Code" />
        <img src={gitlabIcon} alt="GitLab Icon" title="GitLab" />
        <img src={githubIcon} alt="GitHub Icon" title="GitHub" />
      </div>
    </div>

    <h4>Most of my work is owned by the companies I have worked for so I can't show much of that,
        so I will just outline some highlights and then show what I can in the projects section.</h4>

    <div className="work">
      <div className="maintained">
        <h3>Some of the software I have worked on</h3>
        <ul>
          <li>
            A tool to assist in data collection for IT audits
      </li>
          <li>
            A performance and health monitoring system for hundreds of applications and services across a business
      </li>
          <li>
            A real-time telephony monitoring system
      </li>
          <li>
            A suite of desktop applications that communicate with microprocessor controlled products in real-time using bluetooth
      </li>
          <li>
            Many other internal business applications like booking systems, reporting systems and scheduling systems
      </li>
        </ul>
      </div>

      <div className="built">
        <h3>Some of the software I have designed and created</h3>
        <ul>
          <li>
            A cross platform mobile application for communicating with microprocessor controlled medical devices in real-time using bluetooth
      </li>
          <li>
            Web applications to assist in the localisation of mobile application UI text and html help files
      </li>
          <li>
            A micro-service to generate letters and emails automatically for a complaints system
      </li>
          <li>
            A real-time business performance dashboard application
      </li>
          <li>
            Micro-services to monitor the health of application processes
      </li>
        </ul>
      </div>
    </div>
    <h2>Projects</h2>
    <div className="projects">
      <div id="proj1" className="project">
        <h4>Xliff Editor</h4>
        <img src={xliffSite} alt="XLIFF Site Screenshot" title="XLIFF Editor" />
        <div className="project-links">
          <a href="https://gitlab.com/adamflitney/xliff-editor" target="_blank"><span>Code</span></a>
          <a href="https://xliff-editor.netlify.com/#/" target="_blank"><span>Demo</span></a>
        </div>
      </div>
      <div id="proj2" className="project">
      <h4>Personal Site</h4>
        <img src={personalSite} alt="Personal Site Screenshot" title="My Website" />
        <div className="project-links">
          <a href="https://gitlab.com/adamflitney/personal-site-v2" target="_blank"><span>Code</span></a>
          <a href="https://www.adamflitney.com/" target="_blank"><span>Demo</span></a>
        </div>
      </div>
      <div id="proj3" className="project">
      <h4>Pomodoro App</h4>
        <img src={pomodoroApp} alt="Pomodoro App Screenshot" title="Pomodoro App" />
        <div className="project-links">
          <a href="https://gitlab.com/adamflitney/simple-pomo" target="_blank"><span>Code</span></a>
          <a href="https://simple-pomo-6794.netlify.com/" target="_blank"><span>Demo</span></a>
        </div>
      </div>
    </div>

    <h2>Want More?</h2>
    <h4>You can find most of my public code on <a href="https://gitlab.com/adamflitney">GitLab</a>, but I do also occasionally use <a href="https://github.com/adamflitney">GitHub</a>.</h4>
    <h4>I also like to experiment with UI stuff occasionally on <a href="https://codepen.io/blueberrypie/">CodePen</a>. </h4>
    <p className="warning">WARNING: The majority of my code on these sites is not intended to be production ready code. It's mostly just experiments I do to figure something out, or just for fun. It's easily found with a google search anyway so I figured I would jsut include it for the curious.</p>


  </Layout>
)

export default WorkPage
