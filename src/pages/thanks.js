import React from "react";
import { Link } from "gatsby"

import Layout from "../components/layout";
import SEO from "../components/seo";

const ThanksPage = () => (
  <Layout>
    <SEO title="Thanks" />
    <h2>Thanks for getting in touch!</h2>
    <Link
      to="/"
    >
      Return to the main page
    </Link>
  </Layout>
);

export default ThanksPage;