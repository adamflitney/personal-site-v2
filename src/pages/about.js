import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = () => (
  <Layout>
    <SEO title="About" />
    <h2>A little bit about me</h2>
    <p>I am a polyglot programmer with experience in various languages across most areas of the "stack", although I have fallen deeply in love with Javascript and the front-end.</p>
    <p>I have a relatively varied background, working in: software support, task automation, data transformation, desktop development, mobile development, back-end and front-end web development.</p>
    <p>I love learning new things, probably even more than I love solving problems. I am usually watching video courses, reading articles or reading/listening to books about all sorts of topics. I feel this helps greatly with solving problems as I can more easily see the patterns and underlying principles behind a problem by drawing from different fields and topics.</p>
    <p>I also tend to learn new things pretty quickly. This has been pretty useful in my career as I have had to quickly get up to speed with new tools and technologies without much guidance for pretty much every role I have been in.</p>
    <p>When I’m not working or studying, I am normally trying to keep up with my other hobbies. I tend to cycle my focus a bit but my main ones are: martial arts (wing chun kung fu), reading Japanese manga and playing the keyboard/piano (poorly).</p>
  </Layout>
)

export default AboutPage
