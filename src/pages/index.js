import React from "react"

import Layout from "../components/layout"
import MeImage from "../components/meImage"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
      <MeImage/>
    <div>
    
    <h2>Hi! My name is Adam and I solve problems with code!</h2>
    <p>I have worked with various technologies during my career and I firmly believe you should use the right tool for the job, rather than treating everything like a nail just because you are used to using a hammer.</p>
    <p> My passions are learning and creatively solving problems; from writing a script to automate away hours of tedious data manipulation to developing an enterprise business application that saves a company millions a year.</p>
    </div>
    
    
  </Layout>
)

export default IndexPage
